# TrailStash MapSwap

MapSwap is a simple web utility to switch between various webmaps, while preserving the viewport.

It is inspired by MediaWiki's [GeoHack](https://www.mediawiki.org/wiki/GeoHack) and the
[MapSwitcher](https://github.com/david-r-edgar/MapSwitcher) browser extension, but with a desire
for personalization and ease of use.

### Demo
![](./demo.webm)

### Customizing MapSwap

One of the main reasons for the creation of MapSwap was to enable easier customization than the
projects that inspired it.

You can create your own installer to share with others with custom maps by using the `whitelabel.html`
page and setting the `name` & `maps` parameters.

For example:

https://mapswap.trailsta.sh/whitelabel.html?name=Custom%20MapSwap&maps=https://gist.githubusercontent.com/dschep/1a8b0db51b98e57f17ef21797230082f/raw/2aa55322cf43e91bc01b613aa4b076e7ee4205e1/big2.json

##### `name` parameter
This sets the name of the bookmarklet the user will drag to their bookmarks bar.

##### `maps` parameters
This parameter sets what map are avaiable in MapSwap. It must be a JSON file that can be accessed via
HTTP with a [CORS](https://developer.mozilla.org/en-US/docs/Glossary/CORS) request.

It should be an array of objects with keys of `name`, `template` and `icon`. EG:
```
[
  {
    name: "OpenTrailStash",
    icon: "https://open.trailsta.sh/logo.svg",
    template: "https://open.trailsta.sh/#{z}/{y}/{x}",
  }
]
```

##### Template parameters
You'll notice in the example above that the template contains `{x}`, `{y}` and `{z}`. These are used to
generate the link to the the viewport. This is the full list of parameters that get substituted:
* `{x}` - Longitude
* `{x}` - Latitude
* `{z}` - Mapbox & MapLibre style Floating-point Zoom level
* `{Z}` - Google style Floating-point Zoom level (one greater than Mapbox/MapLibre style zoom)
* `{Zr}` - Leaflet style integer Zoom level (one greater than Mapbox/Maplibre style zoom)
* `{zr}` - Integer Zoom level (don't know if any maps use this)
