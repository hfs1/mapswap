// DOM generation helpers

export const h = (elName, props, ...children) => {
  const el = document.createElement(elName);
  for (const key in props) {
    el[key] = props[key];
  }
  for (const child of children) {
    el.appendChild(child);
  }
  return el;
};
export const t = (text) => document.createTextNode(text);
