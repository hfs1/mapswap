const paramIndex = (template, param) => {
  if (param !== "z") {
    return template.indexOf(`{${param}}`);
  }
  for (const z of ["z", "Z", "zr", "Zr"]) {
    const res = template.indexOf(`{${z}}`);
    if (res >= 0) {
      return res;
    }
  }
  return -1;
};

const parseFloats = (xyz) => {
  if (!xyz) return;
  xyz.z = xyz.z && parseFloat(xyz.z);
  xyz.x = parseFloat(xyz.x);
  xyz.y = parseFloat(xyz.y);
  return xyz;
};

export class URLXYZParser {
  constructor(maps) {
    this.maps = maps;
    this.domainTemplates = Object.fromEntries(
      maps.map(({ template }) => [new URL(template).host, template]),
    );
  }
  // fixupZYX takes coordinates that were parsed from a z/y/x-style domain and detect if that
  // default order is incorrect. It does so in a number of ways:
  //  - using the templates of known maps
  //  - values > 90 and < -90 can't be the Y coordinate
  //  - TODO: negative values can't be the zoom level
  //  - TODO: if only one value is an integer or exactly *.0, it is probably the zoom level
  fixupZYX(zyx, url) {
    if (!zyx) return;
    const template = this.domainTemplates[url.host];
    if (template) {
      const zyxSorted = ["z", "y", "x"].sort(
        (a, b) => paramIndex(template, a) - paramIndex(template, b),
      );
      return {
        [zyxSorted[0]]: zyx.z,
        [zyxSorted[1]]: zyx.y,
        [zyxSorted[2]]: zyx.x,
      };
    }
    if (zyx.y > 90 || zyx.y < -90) {
      [zyx.x, zyx.y] = [zyx.y, zyx.x];
    }
    return zyx;
  }
  // fixupLeafletLikeZoom updates the zoom level for sites know to use leaflet-like zoom levels
  fixupLeafletLikeZoom(xyz, url, type) {
    const template = this.domainTemplates[url.host];
    if (xyz && template) {
      if (template.includes("{Z}") || template.includes("{Zr}")) {
        xyz.z -= 1;
      }
    } else if (xyz && type !== "m") {
      // Seems really only maplibre/mapbox are different, everytihng else is like leaflet.
      xyz.z -= 1;
    }
    return xyz;
  }
  parse(urlStr, type) {
    let url;
    try {
      url = new URL(urlStr);
    } catch {
      return;
    }
    // handle clicking MapSwap bookmarklet when on MapSwap
    if (
      url.origin + url.pathname == "https://mapswap.trailsta.sh/" ||
      url.origin + url.pathname ==
        window.location.origin + window.location.pathname
    ) {
      return this.parse(url.searchParams.get("url"));
    }
    for (const method of this.parsers) {
      const res = this.fixupLeafletLikeZoom(method(url), url, type);
      if (res) {
        return res;
      }
    }
  }
  parsers = [
    // geo://{y},{x};z={z} Geo URIs with google's zoom extension
    (url) => {
      if (url.protocol !== "geo:") {
        return;
      }
      const [yx, ...options] = url.pathname.split(";");
      const [y, x] = yx.split(",");
      return {
        y,
        x,
        z: Object.fromEntries(options.map((opt) => opt.split("="))).z,
      };
    },
    // any hash or search containing query params for x, y, and z. They can be in a couple forms
    // and are all case-insensitive:
    // Z | z,zoom,zm,lvl
    // X | x,lon,lng,long,longitude
    // Y | y,lat,latitude
    // Examples:
    // TrailForks      | ?z={z}&lat={y}&lon={x}"
    // WikiMapia       | #z={z}&lat={y}&lon={x}"
    (url) => {
      for (const params of [
        url.searchParams,
        new URLSearchParams(url.hash.slice(1)),
      ]) {
        const xyz = {};
        zKeys: for (const key of ["z", "zoom", "zm", "lvl"]) {
          for (const [urlKey, value] of params) {
            if (urlKey.toLowerCase() == key) {
              xyz.z = value;
              break zKeys;
            }
          }
        }
        xKeys: for (const key of ["x", "lon", "lng", "long", "longitude"]) {
          for (const [urlKey, value] of params) {
            if (urlKey.toLowerCase() == key) {
              xyz.x = value;
              break xKeys;
            }
          }
        }
        yKeys: for (const key of ["y", "lat", "latitude"]) {
          for (const [urlKey, value] of params) {
            if (urlKey.toLowerCase() == key) {
              xyz.y = value;
              break yKeys;
            }
          }
        }
        if (xyz.z && xyz.x && xyz.y) {
          return xyz;
        }
      }
    },
    // Any hash that starts with an zyx delimited by / or ,
    // Examples:
    // mapbox/maplibre's `hash` option | #{z}/{y}/{x}
    // https://osmwd.dsantini.it/      | #{x},{y},{z}
    (url) =>
      this.fixupZYX(
        parseFloats(
          url.hash?.match(
            /^#(?<z>[-0-9.]+)[,\/](?<y>[-0-9.]+)[,\/](?<x>[-0-9.]+)/,
          )?.groups,
        ),
        url,
      ),
    // Any hash or search containing a query params with a value in the form {z}/{y}/{x} or {z},{y},{x}
    // Examples:
    // OpenStreetMap   | #map={z}/{y}/{x}
    // Gravelmap       | #_={z}/{y}/{x}
    // GaiaGPS         | ?loc={z}/{x}/{y}
    // HERE            | ?map={z},{x},{y}
    (url) => {
      for (const params of [
        url.searchParams,
        new URLSearchParams(url.hash.slice(1)),
      ]) {
        for (const [_, v] of params) {
          const zyx = v?.match(
            /^(?<z>[-0-9.]+)[,\/](?<y>[-0-9.]+)[,\/](?<x>[-0-9.]+)/,
          )?.groups;
          if (zyx) return this.fixupZYX(parseFloats(zyx), url);
        }
      }
    },
    // any hash or search containing query params for lat/lon, and z. They can be in a couple forms
    // and are all case-insensitive:
    // Z       | z,zoom,zm,lvl
    // lat&lon | ll,cp,latlon,latlng
    // Additonally, the following delimeters are supported for lat/lon: , ~ /
    // Examples:
    // Bing         | ?&cp={y}~{x}&lvl={z}
    // CalTopo      | #ll={y},{x}&z={z}
    (url) => {
      const xyz = {};
      for (const params of [
        url.searchParams,
        new URLSearchParams(url.hash.slice(1)),
      ]) {
        zKeys: for (const key of ["z", "zoom", "zm", "lvl"]) {
          for (const [urlKey, value] of params) {
            if (urlKey.toLowerCase() == key) {
              xyz.z = value;
              break zKeys;
            }
          }
        }
        xyKeys: for (const key of ["ll", "cp", "latlon", "latlng"]) {
          for (const [urlKey, value] of params) {
            if (urlKey.toLowerCase() == key) {
              const { x, y } = value.match(
                /^(?<y>[-0-9.]+)[,~\/](?<x>[-0-9.]+)$/,
              )?.groups;
              if (y && x) {
                xyz.x = x;
                xyz.y = y;
                break xyKeys;
              }
            }
          }
        }
        if (xyz.z && xyz.x && xyz.y) {
          return xyz;
        }
      }
    },
    // path parts in the form of @{y},{z},{z}z
    // Examples:
    // Google      | /@{y},{x},{z}z/
    // Komoot      | /@{y},{x},{z}z
    (url) =>
      parseFloats(
        url.pathname
          .split("/")
          .map(
            (part) =>
              part.match(/@(?<y>[-0-9.]+),(?<x>[-0-9.]+),(?<z>[-0-9.]+)z/)
                ?.groups,
          )
          .filter((part) => part)[0],
      ),
    // last ditch effort, Any path, hash, or search containing an z/y/x triple
    (url) => {
      for (const str of [url.search, url.hash, url.pathName]) {
        const zyx = str?.match(
          /(?<z>[-0-9.]+)[,\/](?<y>[-0-9.]+)[,\/](?<x>[-0-9.]+)/,
        )?.groups;
        if (zyx) return this.fixupZYX(parseFloats(zyx), url);
      }
    },
  ];
}
