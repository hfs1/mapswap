import { h, t } from "./dom.js";

const defaultFavorites = [
  "OpenStreetMap",
  "Google Maps",
  "Edit OSM with iD",
  "OpenTrailStash",
  "OSM Americana",
];
const favorites = new Set(
  JSON.parse(
    localStorage.getItem("mapswap-favorites") ||
      JSON.stringify(defaultFavorites),
  ),
);

export const fmtURL = (tmpl, { z, x, y }) =>
  tmpl
    .replaceAll("{z}", z)
    .replaceAll("{Z}", z + 1)
    .replaceAll("{zr}", Math.round(z))
    .replaceAll("{Zr}", Math.round(z) + 1)
    .replaceAll("{x}", x)
    .replaceAll("{y}", y);

export const renderNoUrl = () => {
  document
    .getElementById("error")
    .replaceWith(
      h("div", { id: "error" }, h("p", {}, t("No map view detected in URL"))),
    );
};
export const renderResult = (maps, xyz, url, min, currentTag) => {
  if (!xyz) {
    document
      .getElementById("error")
      .replaceWith(
        h(
          "div",
          { id: "error" },
          h("p", {}, t("No map view detected in URL")),
          h("p", {}, h("small", {}, h("a", { href: url }, t(url)))),
        ),
      );
    return;
  }
  const Tags = new Set(["favorites"]);
  for (const { tags } of maps) {
    for (const tag of tags || []) {
      Tags.add(tag);
    }
  }
  if (Tags.size === 1) {
    Tags.clear();
  }
  if (!currentTag) {
    currentTag = Array.from(Tags)[0];
  }
  if (min == null) {
    document
      .getElementById("coords")
      .replaceWith(
        h(
          "div",
          { id: "coords" },
          h(
            "p",
            {},
            t(
              `parsed Zoom(Z)=${xyz.z} / Latitude(Y)=${xyz.y} / Longitude(X)=${xyz.x} from`,
            ),
          ),
          h("p", {}, h("small", {}, h("a", { href: url }, t(url)))),
        ),
      );
  }
  if (!xyz.z) {
    xyz.z = 14;
  }
  document.getElementById("tags").replaceWith(
    h(
      "div",
      { id: "tags" },
      ...Array.from(Tags).map((tag, i) =>
        h(
          "label",
          { for: tag, onclick: () => renderResult(maps, xyz, url, min, tag) },
          h("input", {
            name: "tag",
            value: tag,
            type: "radio",
            checked: tag === currentTag,
          }),
          h("span", {}, t(tag)),
        ),
      ),
    ),
  );
  document.getElementById("maps").replaceWith(
    h(
      "div",
      {
        id: "maps",
        className: "flex one two-700 three-1400 four-2100 center",
      },
      ...maps
        .filter(({ name }) => name)
        .filter(({ name, tags }) => {
          if (currentTag === "favorites") {
            return favorites.has(name);
          } else {
            if (currentTag && tags) {
              return tags.includes(currentTag);
            } else {
              return true;
            }
          }
        })
        .map(({ template, name, icon, tags }, i) =>
          h(
            "div",
            { className: "map" },
            h(
              "a",
              { href: fmtURL(template, xyz), className: "button" },
              h("img", {
                className: "icon",
                src:
                  icon ||
                  "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",
              }),
              t(" " + name),
              Tags.size
                ? h(
                    "span",
                    {
                      className: `favorite ${favorites.has(name) ? "fav" : ""}`,
                      onclick: (e) => {
                        e.preventDefault();
                        if (favorites.has(name)) {
                          favorites.delete(name);
                        } else {
                          favorites.add(name);
                        }
                        localStorage.setItem(
                          "mapswap-favorites",
                          JSON.stringify(Array.from(favorites)),
                        );
                        renderResult(maps, xyz, url, min, currentTag);
                      },
                    },
                    t("⭐"),
                  )
                : t(""),
            ),
          ),
        ),
    ),
  );
};
