export const bookmarklet = (root, extraParams = "") => {
  return `javascript:void(window.open("${root}swap/?${extraParams}type="+(()=>{let q=x=>document.querySelector(x);return q('.leaflet-container')?'l':q('.maplibregl-map')||q('.mapboxgl-map')?'m':q('.ol-viewport')?'ol':''})()+"&url="+encodeURIComponent(window.location)))`;
};
export default bookmarklet;
